package com.example.enryptedstore

data class EncryptionEntry(val encryption: ByteArray, val encryptioniv: ByteArray?)