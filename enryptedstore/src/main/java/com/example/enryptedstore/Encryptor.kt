package com.example.enryptedstore

import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey

class Encryptor(keystore: String) : IEncryptor {

    private val transformation = "AES/GCM/NoPadding"
    private val androidKeyStore = keystore

    private var encryptionIv: ByteArray? = null

    override fun encryptText(alias: String, text: String) : ByteArray {

        val cipher = Cipher.getInstance(transformation)
        cipher.init(Cipher.ENCRYPT_MODE, getSecretKey(alias))

        encryptionIv = cipher.iv

        return cipher.doFinal(text.toByteArray(charset("UTF-8")))
    }

    override fun getIv(): ByteArray? = encryptionIv

    private fun getSecretKey(alias: String) : SecretKey {

        val keyGenerator = KeyGenerator.getInstance("HmacSHA256", androidKeyStore)
        keyGenerator.init(256) // for example

        return keyGenerator.generateKey()
    }
}