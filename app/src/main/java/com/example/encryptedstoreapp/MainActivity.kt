package com.example.encryptedstoreapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.enryptedstore.IEncryptionService
import kotlinx.coroutines.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var encryptionService: IEncryptionService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        GlobalScope.launch {
            initEcryption()
        }

        btn_encrypt.setOnClickListener {
            GlobalScope.launch {
                encryptionService.StoreDecryptedTest("alias", "key", "kamilmiszcz")
            }
        }

        btn_decrypt.setOnClickListener {
            GlobalScope.launch {
                textView.text = encryptionService.GetDecryptedText("alias", "key")
            }
        }
    }

    private suspend fun initEcryption() {
        encryptionService = (applicationContext as ApplicationBase).encryptionService
    }
}
