package com.example.encryptedstoreapp

import android.app.Application
import com.example.enryptedstore.IEncryptionService
import com.example.enryptedstore.MemoryEncryptionService

class ApplicationBase : Application() {

    lateinit var encryptionService: IEncryptionService

    override fun onCreate() {
        super.onCreate()
        encryptionService = MemoryEncryptionService()
    }
}