package com.example.enryptedstore

interface IEncryptor {
    fun encryptText(alias: String, text: String) : ByteArray
    fun getIv() : ByteArray?
}