package com.example.enryptedstore

class EncryptionState {

    private val state: HashMap<String, EncryptionEntry> = HashMap()

    fun register(key: String, value: EncryptionEntry) {
        state[key] = value
    }

    fun get(key: String) = state[key]
}