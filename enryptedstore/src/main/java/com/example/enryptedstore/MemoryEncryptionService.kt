package com.example.enryptedstore

class MemoryEncryptionService : IEncryptionService {

    private val keyStore = "AndroidKeyStore"

    private val encryptionState = EncryptionState()
    private val encryptor: IEncryptor = Encryptor(keyStore)
    private val decryptor: IDecryptor = Decryptor(keyStore)

    override fun GetDecryptedText(alias: String, key: String) : String {
        val encryption = encryptionState.get(key) ?: return ""
        return decryptor.decryptData(alias, encryption.encryption, encryption.encryptioniv)
    }

    override fun StoreDecryptedTest(alias: String, key: String, textToEncrypt: String) {
        val encryption = encryptor.encryptText(alias, textToEncrypt)
        encryptionState.register(key, EncryptionEntry(encryption, encryptor.getIv()))
    }
}