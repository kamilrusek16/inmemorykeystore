package com.example.enryptedstore

interface IDecryptor {
    fun decryptData(alias: String, encryptedData: ByteArray, encryptionIv: ByteArray?) : String
}