package com.example.enryptedstore

interface IEncryptionService {
    fun GetDecryptedText(alias: String, key: String) : String
    fun StoreDecryptedTest(alias: String, key: String, textToEncrypt: String)
}