package com.example.enryptedstore

import java.security.KeyStore
import javax.crypto.Cipher
import javax.crypto.SecretKey
import javax.crypto.spec.IvParameterSpec

class Decryptor(keystore: String): IDecryptor {

    private val TRANSFORMATION = "AES/GCM/NoPadding"
    private val ANDROID_KEY_STORE = keystore

    private val keyStore: KeyStore

    init {
        keyStore = KeyStore.getInstance(ANDROID_KEY_STORE)
        keyStore.load(null)
    }

    override fun decryptData(alias: String, encryptedData: ByteArray, encryptionIv: ByteArray?) : String {
        val cipher = Cipher.getInstance(TRANSFORMATION)
        val spec = IvParameterSpec(encryptionIv)
        cipher.init(Cipher.DECRYPT_MODE, getSecretKey(alias), spec)

        return String(cipher.doFinal(encryptedData), charset("UTF-8"))
    }

    private fun getSecretKey(alias: String): SecretKey
            = (keyStore.getEntry(alias, null) as KeyStore.SecretKeyEntry).secretKey
}